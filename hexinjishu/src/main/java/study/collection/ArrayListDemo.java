package study.collection;

import java.util.ArrayList;

public class ArrayListDemo {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("张三丰");
        list.add("郭靖");
        list.add("杨过");
        System.out.println(list.contains("李莫愁"));
        list.remove(0);
        System.out.println("====================1111");
        for (int i = 0; i <list.size() ; i++) {
            String name = list.get(i);
            System.out.println(name);
        }
        System.out.println("---");
        list.forEach(name-> System.out.println(name));
        System.out.println("====================2222");
        System.out.println(list.indexOf("小龙女"));
        list.clear();
        System.out.println("====================3333");
        list.forEach(name-> System.out.println(name));
        for (Object obj:list
             ) {
            String name = (String) obj;
            System.out.println(name+"0000");
        }
        System.out.println(list.isEmpty());
    }

}
