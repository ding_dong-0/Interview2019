package study.collection;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Hello {
    public static void main(String[] args) {
        Vector<Object> vector = new Vector<>();
        List<Object> synchronizedList = Collections.synchronizedList(new ArrayList<>());
        CopyOnWriteArrayList<String> copyOnWriteArrayList = new CopyOnWriteArrayList<>();
        ArrayList<String> arrayList = new ArrayList<>();
        add(vector);
        add(synchronizedList);
        add(copyOnWriteArrayList);
        add(arrayList);
        System.out.println("===========++++++++++++++");
        get(vector);
        get(synchronizedList);
        get(copyOnWriteArrayList);
        get(arrayList);


    }

    public static void add(List list) {
        long start = System.currentTimeMillis();//当前时间
        for (int i = 0; i < 100000; i++) {
            list.add(i);
        }
        long end = System.currentTimeMillis();
        System.out.println(list.getClass().getName() + "     .size(大小为)： " + list.size() + " ,add耗时： " + (end - start) + " ms ");
    }

    public static void get(List list) {
        long start = System.currentTimeMillis();//当前时间
        for (int i = 0; i < list.size(); i++) {
            Object o = list.get(i);
        }
        long end = System.currentTimeMillis();
        System.out.println(list.getClass().getName() + "     .size(大小为)： " + list.size() + " ,get耗时： " + (end - start) + " ms ");
    }
}
