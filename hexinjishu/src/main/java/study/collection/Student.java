package study.collection;

import lombok.Data;

@Data
public class Student implements Comparable {
    private int number = 0;  //学号
    private String name = "";  //姓名
    private String gender = "";//性别

    @Override
    public int compareTo(Object obj) {
        Student student = (Student) obj;
        if (this.number == student.number) {
            return 0;
        }else if (this.number>student.getNumber()){
            return 1;
        }else {
            return -1;
        }
    }
}
