package study.getclassdemo;


public class GetClassDemo02 {
    public static void main(String[] args) {
        Class<?> c1 = null;   //指定泛型
        Class<?> c2 = null;
        Class<?> c3 = null;
        try {
            c1 = Class.forName("study.getclassdemo.X");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        c2 = new X().getClass();
        c3 = X.class;
        System.out.println("类的名称：" + c1.getName());
        System.out.println("类的名称：" + c2.getName());
        System.out.println("类的名称：" + c3.getName());

    }


}
