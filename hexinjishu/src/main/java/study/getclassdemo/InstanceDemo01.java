package study.getclassdemo;

import lombok.Data;

@Data
class Person {
    private String name;
    private int age;
}

public class InstanceDemo01 {
    public static void main(String[] args) {
        Class<?> c = null;
        try {
            c = Class.forName("study.getclassdemo.Person");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Person per = null;
        try {
            per = (Person) c.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        per.setName("李兴华");
        per.setAge(30);
        System.out.println(per);
    }
}

