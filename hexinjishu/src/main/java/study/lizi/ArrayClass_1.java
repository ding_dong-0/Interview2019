package study.lizi;

public class ArrayClass_1 {
    public static void main(String[] args) {
        int i;
        int array[];
        array = new int[10];
        System.out.println("数组的长度是：" + array.length);
        for (i = 0; i < 10; i++) {
            System.out.print("a[" + i + "]=" + array[i] + ",");
            if (i % 2 == 1) {
                System.out.println();
            }
        }
    }
}
