package study.lizi;

public class BB {
    private static int num = 20;
    private static String name = "vivi";

    public void call() {
        BB.print();
        new BB().print();
        System.out.println(BB.name + "," + BB.num);
    }

    private static void print() {
        System.out.println("Hell World");
    }

    public static void main(String[] args) {
        BB bb = new BB();
        bb.call();
        System.out.println("===================");
        int x = 12;
        int y = 5;
        System.out.println(x / y);
        System.out.println("===================");
        System.out.println("y=" + (++y));//前置运算
        System.out.println("x=" + (x++));//后置运算
        System.out.println("===================");
        int a = 10;
        int b = 7;
        b = --a;
        System.out.println("b=" + b);
    }

}
