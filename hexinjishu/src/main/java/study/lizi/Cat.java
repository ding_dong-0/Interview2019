package study.lizi;

public class Cat {
    private String name;

    public Cat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        Cat c1 = new Cat("王磊");
        Cat c2 = new Cat("王磊");
        System.out.println(c1.equals(c2));//false
        System.out.println("===================");
        String s1 = new String("老王");
        String s2 = new String("老王");
        System.out.println(s1.equals(s2)); // true
    }


}
