package study.lizi;

public interface D {
}

class A {
}

class B extends A implements D {
}

class C extends B {
    public static void main(String[] args) {
        A a = new A();
        B b = new B();
        C c = new C();
        if (a instanceof B) {
            System.out.println("Hello vivi");
        }
        if (b instanceof A) {
            System.out.println("Hello cici");
        }
        if (c instanceof C) {
            System.out.println("Hello wiwi");
        }
        if (c instanceof D) {
            System.out.println("Hello ");
        }
    }
}