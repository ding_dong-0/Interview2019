package study.lizi;

public class Demo {
    public void demo() {
        int a, b, c;
        a = 100;
        while (a > 0) {
            b = a * (a - 1);
            System.out.println("b=" + b);
//            c = c + 1;  //c 没有初始化
            a--;
        }
    }

    public static void main(String[] args) {
        Demo demo = new Demo();
        demo.demo();

    }
}
