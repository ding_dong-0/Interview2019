package study.lizi;

/**
 * 静态初始化器
 * static{和}之间的代码被称为静态初始化器。它只有在第一次加载类时运行。只有静态变量才可以在静
 * 态初始化器中进行访问。虽然创建了三个实例，但是静态初始化器只运行一次
 */
public class InitialiizerExamples {
    static int count;
    int i;

//    static
    {
        System.out.println("static Initialiizer");
        i = 6;
        count = count + 1;
        System.out.println("count when Static Initializer is run is " + count);
    }

    public static void main(String[] args) {
        InitialiizerExamples example = new InitialiizerExamples();
        InitialiizerExamples example1 = new InitialiizerExamples();
        InitialiizerExamples example2 = new InitialiizerExamples();
    }
}
