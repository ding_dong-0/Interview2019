package study.lizi;

import javax.xml.crypto.Data;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

public class Lph {
    public static void main(String[] args) {
        Date date = new Date();
        add(date);

//        tokenize("ac;bd;mm;ko;ll", ";");
//        tokenizeUsingScanner("ac;bd;mm;ko;ll", ";");
    }

    public static void add(Date date) {
        date.setTime(date.getTime()+6*60*60*1000);//增加6个小时
        System.out.println(DateFormat.getInstance().format(date));
        System.out.println(date);

        date.setTime(date.getTime()-6*60*60*1000);
        System.out.println(DateFormat.getInstance().format(date));
        System.out.println(date);
    }

    public static void tokenize(String string, String regex) {
        String[] tokens = string.split(regex);
        System.out.println(Arrays.toString(tokens));
    }
    public static void tokenizeUsingScanner(String string, String regex) {
        Scanner scanner = new Scanner(string);
        scanner.useDelimiter(regex);
        ArrayList<String> matches = new ArrayList<>();
        while (scanner.hasNext()){
            matches.add(scanner.next());
        }
        System.out.println(matches);
    }
}
