package study.lizi;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class MapTest {
    public static void main(String[] args) {

        Map<String, String> map = new TreeMap<>(new Comparator<String>() {
            @java.lang.Override
            public int compare(String o1, String o2) {
                String param1 = (String) o1;
                String param2 = (String) o2;
                return -param1.compareTo(param2);
            }
        });

        //初始化定义一个比较器
        MyComparator comparator = new MyComparator();
        Map<String, String> map1 = new TreeMap<String, String>(comparator);
        map.put("a", "a");
        map.put("b", "b");
        map.put("f", "f");
        map.put("d", "d");
        map.put("c", "c");
        map.put("g", "g");
        Iterator<String> iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            System.out.println(map.get(key));
        }
    }

    static class MyComparator implements Comparator {
        @java.lang.Override
        public int compare(Object o1, Object o2) {
            String param1 = (String) o1;
            String param2 = (String) o2;
            return -param1.compareTo(param2);
        }
    }
}
