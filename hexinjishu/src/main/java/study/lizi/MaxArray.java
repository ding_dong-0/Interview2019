package study.lizi;

public class MaxArray {
    public static void main(String[] args) {
        int arry[] = new int[10];
        setValue(arry);

        showValue(arry);
        int maxValue = getMaxValue(arry);
        System.out.println("\n最大值为：" + maxValue);
    }

    public static int getMaxValue(int[] a) {
        int max = a[0];
        for (int i = 1; i < a.length; i++) {
            if (a[i] > max) {
                max = a[i];
            }
        }
        return max;
    }

    /**
     * 将数组元素赋随机值
     *
     * @param a
     */
    public static void setValue(int[] a) {
        for (int i = 0; i < a.length; i++) {
            a[i] = (int) (Math.random() * 100);
        }
    }

    /**
     * 遍历数组的元素
     *
     * @param a
     */
    public static void showValue(int[] a) {
        System.out.println("数组元素值为：");
        for (int i = 0; i < a.length; i++) {
            System.out.print(" " + a[i]);
            if ((i + 1) % 5 == 0) {
                System.out.println();
            }
        }
    }
}
