package study.lizi;

public class MyClass {
    public static void main(String[] args) {
        String[] myChars = {"a", "b", "c", "d", "e", "f"};
        if (args.length == 0) {
            System.out.println("no result");
        } else {
            System.out.println(myChars[args.length] + " result");
        }
    }
}
