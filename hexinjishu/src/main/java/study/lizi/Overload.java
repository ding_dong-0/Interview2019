package study.lizi;

public class Overload {
    public static void main(String[] args) {
        Matharea ma = new Matharea();
        float area = ma.area(5, 11.2, 5);
        System.out.println(area);
    }
}

class Matharea {   //创建一个类
    public int area(int lang, int width) {//求四边形面积
        int rectangular = lang * width;
        return rectangular;
    }

    public float area(double top, double bottom, double high) {  //求梯形的面积
        float trapezoidal = (float) ((top + bottom) * high / 2);
        //进行强制类型转换，将double转换成float
        return trapezoidal;
    }

    public String area(int r) {                  //求圆的面积
        float circular = (float) (3.14 * r * r); //进行强制类型转换，将double转换成float
        return "这是圆的面积：" + circular;//返回结果值
    }
}