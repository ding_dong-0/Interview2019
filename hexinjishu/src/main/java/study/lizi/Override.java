package study.lizi;

public class Override {
    public static void main(String[] args) {
        Person1 person = new Woman();
        person.foot();
        person.talk();
    }
}

class Person1 {
    public void talk() {
        System.out.println("Person.talk()");
    }

    public void foot() {
        System.out.println("Person.foot()");
    }
}

class Woman extends Person1 {

    public void talk() {
        System.out.println("Woman.talk()");
    }

    public void foot() {
        System.out.println("Woman.foot()");
    }
}
