package study.lizi;

import java.util.*;

/**
 * list中存放对象类型的元素需要重写 hashCode 和 equals 方法
 */
public class People {
    private String name;
    private String phoneNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public People(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    @java.lang.Override
    public String toString() {
        return "People{" +
                "name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }

    @java.lang.Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        People people = (People) o;
        return Objects.equals(name, people.name) &&
                Objects.equals(phoneNumber, people.phoneNumber);
    }

    @java.lang.Override
    public int hashCode() {
        return Objects.hash(name, phoneNumber);
    }

    public static void main(String[] args) {
        List<People> listPeople = new ArrayList<>();
        listPeople.add(new People("张三","11111"));
        listPeople.add(new People("张三","22222"));
        listPeople.add(new People("李四","33333"));
        listPeople.add(new People("李四","33333"));
        listPeople.add(new People("李四","33333"));
        listPeople.add(new People("张三","22222"));
        Set<People> setData = new HashSet<>();
        setData.addAll(listPeople);
        System.out.println("list: "+listPeople.toString());
        System.out.println("set:"+setData.toString());
    }
}
