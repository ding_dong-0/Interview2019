package study.lizi;

import java.util.Arrays;
import java.util.stream.IntStream;

public class ShuZuQuChong {
    public static void main(String[] args) {
        int[] arr = {1, 1, 1, 1, 2, 5, 2, 45, 6, 23, 6, 3, 4, 3, 6, 5, 1};
        int[] ints = Arrays.stream(arr).distinct().toArray();
        for (int i = 0; i < ints.length; i++) {//for 循环打印出数组
            System.out.println(ints[i]);
        }
        fenKaiQuChong(arr);
    }

    public static void fenKaiQuChong(int[] arr) {
        IntStream stream = Arrays.stream(arr);//将数组转化为Stream流
        IntStream distinct = stream.distinct();//Stream流去重
        int[] ints = distinct.toArray();//Stream流转化为数组
        for (int i = 0; i < ints.length; i++) {//for 循环打印出数组
            System.out.println(ints[i]);
        }
    }
}
