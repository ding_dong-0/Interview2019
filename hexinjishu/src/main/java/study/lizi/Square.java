package study.lizi;

/**
 * x的n次方计算
 */
public class Square {
    public static void main(String[] args) {
        int power = 3;
        double truth = 10;
        System.out.println(square(power, truth));//表示10的3次方的计算结果
    }

    /**
     * x的n次方计算
     *
     * @param power 指数幂
     * @param truth 底数
     * @return
     */

    public static double square(int power, double truth) {
        double result = 0;
        for (int i = 0; i < power; i++) {
            if (result != 0) {
                result = result * truth;
            } else {
                result = truth;
            }
        }
        return result;
    }
}
