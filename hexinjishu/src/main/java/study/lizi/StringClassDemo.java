package study.lizi;

public class StringClassDemo {
    public static void main(String[] args) {
        String str1 = "str";
        String str2 = "string";
        String str3 = "ing";
        String str4 = str1 + str3;
        str1 = str4;
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str1 == str2);//str1 和 str2 对象不同，内容相同
        System.out.println("str1" + ((str1 == str2) ? "==" : "!=") + "str2");
    }
}
