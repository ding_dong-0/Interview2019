package study.lizi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        /**
         * 将 list 转化为数组
         * list to array
         */
        ArrayList list = new ArrayList<>();
        list.add("王磊");
        list.add("的博客");
        Object[] arrays = list.toArray();
        //打印出数组 内容
        for (int i = 0; i < arrays.length; i++) {
            System.out.println(arrays[i]);
        }
        /**
         * 数组转化为 list
         * array to list
         */
        String[] as = {"aa", "bb"};
        List<Object> list1s = Arrays.asList(as);
        //打印出list 内容
        list1s.forEach(l -> System.out.println(l));
    }
}
