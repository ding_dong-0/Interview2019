package study.lizi;

public class Test2 {
    public static void main(String[] args) {
        // StringBuffer reverse 字符串反转
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("abcdefg");
        stringBuilder.reverse();
        System.out.println(stringBuilder);//gfedcba
        // StringBuilder reverse 字符串反转
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("abcdefg");
        stringBuffer.reverse();
        System.out.println(stringBuffer);
    }
}
