package study.lizi;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * list 去重
 * set 集合没有重复的元素
 */
public class TestListQuChong {
    public static void main(String[] args) {
        List<Object> list = new ArrayList<>();
        list.add(11);
        list.add(12);
        list.add(13);
        list.add(14);
        list.add(15);
        list.add(11);
        System.out.println(list);
        HashSet<Object> set = new HashSet<>();
        set.addAll(list);//将 list 中的元素都添加到 set 中
        System.out.println(set);
    }
}
