package study.lizi;

/**
 * 可变参数
 */
public class VariableArgumentExamples {
    public int sum(int... numbers) {
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        return sum;
    }

    public static void main(String[] args) {
        VariableArgumentExamples example = new VariableArgumentExamples();
        System.out.println(example.sum(1, 4, 5));
        System.out.println(example.sum(1, 4, 5, 20));
        System.out.println(example.sum());
    }
}
