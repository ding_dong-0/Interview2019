package study.scsbh;

public class LightControl {

    /**
     * 根据返回的16进制指令判断单灯执行的情况
     *
     * @param fhz 返回的指令
     * @return
     */
    public static String lightResult(String fhz) {
        String shebeibianhao = fhz.substring(10, 18);
        String kgzt = fhz.substring(19, 20);
        if (kgzt.equals("0")) {
            return "设备编号:" + shebeibianhao + "关灯成功";
        } else if (kgzt.equals("1")) {
            return "设备编号:" + shebeibianhao + "关灯失败";
        } else if (kgzt.equals("2")) {
            return "设备编号:" + shebeibianhao + "通信失败";
        } else {
            return null;
        }

        //        String st1 = "5a088000a14000085100ba01";
//        String st1= "5a088000a14000085101ba01";
//        String st1 = "5a088000a14000085102ba01";
        /*
        String shebeibianhao = fhz.substring(10, 18);
        String kgzt = fhz.substring(19, 20);
        System.out.println(kgzt);
        if (kgzt.equals("0")) {
            String s = "设备编号:" + shebeibianhao + "关灯成功";
            System.out.println(s);
            return s;
        } else if (kgzt.equals("1")) {
            String s = "设备编号:" + shebeibianhao + "关灯失败";
            System.out.println(s);
            return s;

        } else if (kgzt.equals("2")) {
            String s = "设备编号:" + shebeibianhao + "通信失败";
            System.out.println(s);
            return s;
        } else {
            return null;
        }

         */

    }


    /**
     * @param s1 设备编号
     * @param s2 状态码（0 开灯，1 关灯）
     * @return
     */
    public static String OpenOff(String s1, String s2) {
        if (s2.equals("1")) {//开灯
            s2 = "64";
            return sczl(s1, s2);
        } else if (s2.equals("0")) {//关灯
            s2 = "00";
            return sczl(s1, s2);
        } else {
            return null;
        }
    }


    /**
     * 生成要发送给单灯控制器的指令
     *
     * @param s1 设备编号
     * @param s2 指令中和开关相关的16 进制字符
     * @return
     */
    public static String sczl(String s1, String s2) {
        String s = "5A0A0000A1";
        s = s + s1;
        s = s + "0101" + s2;
        String substring = s.substring(4, 24);
        String s3 = makeChecksum(substring);
        s3 = s3.substring(s3.length() - 2, s3.length());
        String s4 = s + s3 + "01";
        s4 = s4.toUpperCase();
        return s4;
    }


    /**
     * 16进制字符串求和
     *
     * @param hexdata 给定的16进制的字符串
     * @return
     */
    public static String makeChecksum(String hexdata) {
        if (hexdata == null || hexdata.equals("")) {
            return "00";
        }
        hexdata = hexdata.replaceAll(" ", "");
        int total = 0;
        int len = hexdata.length();
        if (len % 2 != 0) {
            return "00";
        }
        int num = 0;
        while (num < len) {
            String s = hexdata.substring(num, num + 2);
            total += Integer.parseInt(s, 16);
            num = num + 2;
        }
        return hexInt(total);
    }

    private static String hexInt(int total) {
        int a = total / 256;
        int b = total % 256;
        if (a > 255) {
            return hexInt(a) + format(b);
        }
        return format(a) + format(b);
    }

    private static String format(int hex) {
        String hexa = Integer.toHexString(hex);
        int len = hexa.length();
        if (len < 2) {
            hexa = "0" + hexa;
        }
        return hexa;
    }

}
