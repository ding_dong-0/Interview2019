package study.scsbh;

import org.apache.commons.lang.StringUtils;

public class Sczl {
    /**
     * @param s1 设备编号
     * @param s2 状态码（0 开灯，1 关灯）
     * @return
     */
    public static String sc(String s1, String s2) {


        if (s2.equals("0")) {//关灯
            s2 = "64";
            String s = sczl(s1, s2);
//            System.out.println("关灯指令" + s);
            return s;

        } else if (s2.equals("1")) {//开灯
            s2 = "00";
            String s = sczl(s1, s2);
//            System.out.println("开灯指令" + s);
            return s;
        } else {
            return null;
        }
    }


    public static String sczl(String s1, String s2) {
        String s = "5A0A0000A1";
        s = s + s1;
        s = s + "0101" + s2;
        String substring = s.substring(4, 24);
        String s3 = QhUtuil.makeChecksum(substring);
        String s5 = s3.substring(s3.length() - 2, s3.length());
        String s4 = s + s5 + "01";
        s4 = s4.toUpperCase();
        return s4;


        // 原始  5A0A0000A140000851010164A001
//              5A0A0000A140000851010164A001

//           5A0A0000A140000851010164
//               0000A1400008516
        // 原始  5A0A0000A1400008510101003C01

        //      5A0A0000A1400008510101003C01
//           5A0A0000A140000851010100
//        String s1 = "40000851";
//        String s2 = "64";
//        String s2="00";
//         下边是生成的逻辑测试
        /*
        String sq = "5A0A0000A1";
        String s = sq + s1;
        s = s +"0101"+ s2;
        System.out.println(s);
        String substring = s.substring(4, 24);
        String s3 = QhUtuil.makeChecksum(substring);
        String s5 = s3.substring(s3.length() - 2, s3.length());
        System.out.println(s3);
        String s4 = s + s5+"01";
        s4=s4.toUpperCase();
        System.out.println(s4);
        return s4;
        */

    }
}
