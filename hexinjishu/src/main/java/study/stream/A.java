package study.stream;

import java.util.Random;

public class A {
    public static void main(String[] args) {
        new Random().ints(10)
                .map(i -> Math.abs(i))
                .forEach(i -> System.out.println(i));
        System.out.println("====================================");
        new Random().ints(5).map(Math::abs).forEach(System.out::println);
    }
}
