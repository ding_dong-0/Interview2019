package study.stream;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectingIntoMaps {

    @Getter
    @AllArgsConstructor
    @ToString
    public static class Person {
        private int id;
        private String name;
    }

    public static Stream<Person> people() {
        return Stream.of(
                new Person(1001, "Peter"),
                new Person(1002, "Paul"),
                new Person(1003, "Mary")
        );
    }

    public static void main(String[] args) {
        Map<Integer, String> idToName = people().collect(Collectors.toMap(Person::getId, Person::getName));
        System.out.println("idToName:" + idToName);

        Map<Integer, Person> idToPerson = people().collect(Collectors.toMap(Person::getId, Function.identity()));
        System.out.println("idToPerson:" + idToPerson.getClass().getName() + idToPerson);
        System.out.println("====================================");
        TreeMap<Integer, Person> idToPerson2 = people().collect(Collectors.toMap(Person::getId, Function.identity(), (existingValue, newValue) -> {
            throw new IllegalStateException();
        }, TreeMap::new));
        System.out.println("idToPerson2:" + idToPerson2.getClass().getName() + idToPerson2);
        System.out.println("====================================");

        Stream<Locale> locales = Stream.of(Locale.getAvailableLocales());
        Map<String, String> languageNames = locales.collect(Collectors.toMap(
                Locale::getDisplayLanguage,
                l -> l.getDisplayLanguage(l),
                (existingValue, newValue) -> existingValue));
        System.out.println("languageNames" + languageNames);

        System.out.println("====================================");
        Stream<Locale> locales2 = Stream.of(Locale.getAvailableLocales());
        Map<String, Set<String>> countryLanguageSets = locales2.collect(Collectors.toMap(Locale::getCountry, l -> Collections.singleton(l.getDisplayLanguage()),
                (a, b) -> {
                    HashSet<String> union = new HashSet<>();
                    union.addAll(b);
                    return union;
                }));
        System.out.println("countryLanguageSets:" + countryLanguageSets);


    }
}
