import lombok.extern.slf4j.Slf4j;
import study.collection.Student;

import study.scsbh.LightControl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

@Slf4j
public class Test {
    @org.junit.Test
    public void test() {
        ArrayList<Object> list = new ArrayList<>();
        list.add("张三");
        list.add("李四");
        list.add("王五");
        list.add(2, "杰伦");
        System.out.println("使用Iterator遍历，分别是：");
        Iterator<Object> it = list.iterator();
        while (it.hasNext()) {
            String name = (String) it.next();
            System.out.println(name);
        }
    }

    @org.junit.Test
    public void test1() {
        ArrayList<String> list = new ArrayList<>();
        list.add("张三丰");
        list.add("杨过");
        list.add("郭靖");
        Collections.fill(list, "东方不败"); //替换元素
        list.forEach(n -> System.out.println(n));
        System.out.println("===");
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String name = iterator.next();
            System.out.println(name);
        }

    }

    @org.junit.Test
    public void test3() {
        HashMap<String, Student> students = new HashMap<>();
    }

    @org.junit.Test
    public void test4() {
        String st1 = "5a088000a14000085100ba01";
        String st2 = "5a088000a14000085101ba01";
        String st3 = "5a088000a14000085102ba01";
        String shebeibianhao = st1.substring(10, 18);
        String kgzt = st1.substring(19, 20);
        System.out.println(kgzt);
        if (kgzt.equals("0")) {
            System.out.println("shebeibianhao:" + shebeibianhao + "关灯成功");
        } else if (kgzt.equals("1")) {
            System.out.println("shebeibianhao:" + shebeibianhao + "开灯成功");
        }
        System.out.println("");
        System.out.println(shebeibianhao);
    }


    //根据返回的字符串获取里边的编号以及根据特定的返回片段排断智慧灯杆的状态，状态截取指定的字符串，
    public static void main(String[] args) {
        String st1 = "5a088000a14000085100ba01";
        String st = "5a088000a14000085100ba01";
        String shebeibianhao = st.substring(10, 18);
        String kgzt = st.substring(19, 20);
        System.out.println(kgzt);
        if (kgzt.equals("0")) {
            System.out.println("shebeibianhao:" + shebeibianhao + "关灯成功");
        } else if (kgzt.equals("1")) {
            System.out.println("shebeibianhao:" + shebeibianhao + "开灯成功");
        }
        System.out.println("");
        System.out.println(shebeibianhao);
    }





}
