package bat.ke.qq.com;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Callable01 implements Callable<String> {

    @Override
    public String call() throws Exception {
        return "你好，世界";
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask task = new FutureTask(new Callable01());
        new Thread(task).start();//多线程：start();>>run方法
        System.out.println(task.get());
    }
}
