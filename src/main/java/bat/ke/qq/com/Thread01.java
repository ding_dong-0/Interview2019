package bat.ke.qq.com;


import java.util.concurrent.TimeUnit;

public class Thread01 extends Thread {
    @Override
    public void run() {
        System.out.println("源码学院");
    }

    public static void main(String[] args) {
        new Thread01().run();//方法级别的调用,只启动了一个main线程

        new Thread01().start(); //通过start方法启动的线程，除了main线程外，还启动了一个线程
        try {
            TimeUnit.SECONDS.sleep(Long.MAX_VALUE);} catch (InterruptedException e) {e.printStackTrace();}
    }

}
