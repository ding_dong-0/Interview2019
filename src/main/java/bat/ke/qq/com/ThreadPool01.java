package bat.ke.qq.com;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionHandler;


/**
 * submit  execute区别？？？
 * 1.submit是由返回值的， execute是没有返回值的
 *
 */
public class ThreadPool01  {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
//        executorService.submit(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("你好，世界");
//            }
//        });

        executorService.execute(new Runnable(){
            @Override
            public void run() {
                System.out.println("你好，我来自execute方法世界");
            }
        });
//        //Lamda表达式的实现
//        executorService.submit(() -> {
//            System.out.println("我来自Lamda表达式");
//        });
        executorService.shutdown();
    }
}
