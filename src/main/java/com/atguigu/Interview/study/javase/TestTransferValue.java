package com.atguigu.Interview.study.javase;

import com.atguigu.Interview.entities.Person;

public class TestTransferValue {
    public void changValue1(int age){
        age=30;
    }
    public void changValue2(Person person){
        person.setPersonName("xxx");
    }
    public void changValue3(String str){
        str ="xxx";
    }

    public static void main(String[] args) {
        TestTransferValue test = new TestTransferValue();
        int age=20;
        test.changValue1(age);
        System.out.println("age----"+age);

        Person person=new Person("abc");
        test.changValue2(person);
        System.out.println("personName-----"+person.getPersonName());

        String str=new String("abc");//"abc";
        test.changValue3(str);
        System.out.println("String----"+str);
    }
}
