package com.atguigu.Interview.study.jvm.oom;

import java.util.Random;

/**
 * 堆内存溢出（创建的对象太多了）
 */

public class JavaHeapSpaceDemo {
    public static void main(String[] args) {
        String str="atguigu";
        while (true){
            str+=str+new Random().nextInt(11111111)+new Random().nextInt(11111111);
            str.intern();//Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
        }
    }
}
