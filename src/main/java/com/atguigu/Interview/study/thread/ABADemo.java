package com.atguigu.Interview.study.thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicStampedReference;

public class ABADemo { //ABA问题的解决     AtomicStampedReference专门解决ABA问题（带时间戳的原子引用）
    static AtomicReference<Integer> atomicReference = new AtomicReference<>(100);//普通的原子引用User如何原子引用
    static AtomicStampedReference<Integer> atomicStampedReference = new AtomicStampedReference<>(100, 1);

    public static void main(String[] args) {
        //ABA问题的产生
        ABAproduct();

        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //ABA问题的解决
        ABASolution();
    }


    //ABA问题的解决
    public static void ABASolution() {
        System.out.println("=======================以下是ABA问题的解决========================");
        new Thread(() -> {
            int stamp = atomicStampedReference.getStamp();
            //暂停1秒tt2线程，保证tt2的线程取得版本号的值
            System.out.println(Thread.currentThread().getName() + "\t 第1次版本号是： " + stamp);
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            atomicStampedReference.compareAndSet(100, 101, stamp, atomicStampedReference.getStamp() + 1);
            System.out.println(Thread.currentThread().getName() + "\t 第2次版本号" + atomicStampedReference.getStamp());
            atomicStampedReference.compareAndSet(101, 100, atomicStampedReference.getStamp(), atomicStampedReference.getStamp() + 1);
            System.out.println(Thread.currentThread().getName() + "\t 第3次版本号" + atomicStampedReference.getStamp());
        }, "tt1").start();

        new Thread(() -> {
            int stamp = atomicStampedReference.getStamp();
            System.out.println(Thread.currentThread().getName() + "\t 第1次版本号是： " + stamp);
            //暂停3秒tt2线程，保证tt2线程完成一次ABA操作
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean b = atomicStampedReference.compareAndSet(100, 2019, stamp, atomicStampedReference.getStamp() + 1);
            System.out.println(Thread.currentThread().getName() + "\t修改成功否： " + b + "\t当前实际最新版本号：" + atomicStampedReference.getStamp());
            System.out.println(Thread.currentThread().getName() + "\t 当前实际最新值：" + atomicStampedReference.getReference());
        }, "tt2").start();
    }


    //ABA问题的产生
    public static void ABAproduct() {
        System.out.println("=======================以下是ABA问题的产生========================");
        new Thread(() -> {
            atomicReference.compareAndSet(100, 101);
            atomicReference.compareAndSet(101, 100);
        }, "t1线程").start();
        new Thread(() -> {
            //暂停1秒t2线程，保证上面的线程完成了一次ABA操作
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(atomicReference.compareAndSet(100, 2019) + "\t" + atomicReference.get());
        }, "t2线程").start();
    }

}
