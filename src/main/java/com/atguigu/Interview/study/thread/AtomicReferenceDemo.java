package com.atguigu.Interview.study.thread;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@AllArgsConstructor
@Data
@ToString
class User {
    String userName;
    int age;
}


public class AtomicReferenceDemo {
    public static void main(String[] args) {
        //原子类的整型 AtomicInteger(java提供) 怎样生成原子类型的User/Order/Customer/Student


        User z3=new User("z3",22);
        User l4=new User("l4",25);
        AtomicReference<User> atomicReference=new AtomicReference<>();
        atomicReference.set(z3);

        System.out.println(atomicReference.compareAndSet(z3, l4)+"\t"+atomicReference.get().toString());
        System.out.println(atomicReference.compareAndSet(z3, l4)+"\t"+atomicReference.get().toString());
    }
}
