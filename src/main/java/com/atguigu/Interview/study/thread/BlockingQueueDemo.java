package com.atguigu.Interview.study.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * ArrayBlockingQueue:是一个基于数组结构的有界阻塞队列，此队列按FIO(先进先出)原则对元素进行排序
 * LinkedBlockingDeque:一个基于链表结构的阻塞队列，此队列按FIFO(先进先出)排列元素，吞吐量通常高于ArrayBlockingQueue
 * SynchronousQueue:一个不存储元素的阻塞队列，每个插入操作必须等到另一个线程调用移除操作，否则插入操作一直处于阻塞状态吞吐量通常要高
 * <p>
 * 1.队列
 * <p>
 * <p>
 * 2.阻塞队列
 * 2.1 阻塞队列有没有好的一面
 * <p>
 * 2.2 不得不阻塞，如何管理
 */
public class BlockingQueueDemo {
    public static void main(String[] args) throws InterruptedException {
//        List list=new ArrayList();
//        blockQueueAdd();
        blockingQueueOffer();
//        blockingQueuePut();
//        BlockingQueue<String> blockingQueue=new ArrayBlockingQueue<>(3);
//        System.out.println(blockingQueue.offer("a", 2L, TimeUnit.SECONDS));
//        System.out.println(blockingQueue.offer("b", 2L, TimeUnit.SECONDS));
//        System.out.println(blockingQueue.offer("c", 2L, TimeUnit.SECONDS));
//        System.out.println(blockingQueue.offer("d", 2L, TimeUnit.SECONDS));
    }

    public static void blockingQueuePut() throws InterruptedException {
        BlockingQueue<String> blockingQueue=new ArrayBlockingQueue<>(3);
        blockingQueue.put("a");
        blockingQueue.put("b");
        blockingQueue.put("c");
        System.out.println("==================");
//        blockingQueue.put("c");
        blockingQueue.take();
        blockingQueue.take();
        blockingQueue.take();
//        blockingQueue.take();
    }

    public static void blockingQueueOffer() {
        BlockingQueue<String> blockingQueue=new ArrayBlockingQueue<>(3);
        System.out.println(blockingQueue.offer("a"));
        System.out.println(blockingQueue.offer("b"));
        System.out.println(blockingQueue.offer("c"));
        System.out.println(blockingQueue.offer("d"));

        System.out.println(blockingQueue.peek());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
    }


    public static void blockQueueAdd() {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);//必须设置大小
        System.out.println(blockingQueue.add("a"));
        System.out.println(blockingQueue.add("b"));
        System.out.println(blockingQueue.add("c"));
//        System.out.println(blockingQueue.add("e"));//java.lang.IllegalStateException: Queue full

        //检查队列空不空，并返回队首元素是谁，
        System.out.println(blockingQueue.element());


        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
//        System.out.println(blockingQueue.remove());//java.util.NoSuchElementException
    }
}
