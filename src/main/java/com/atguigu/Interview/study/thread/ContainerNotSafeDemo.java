package com.atguigu.Interview.study.thread;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 集合类不安全的问题
 */

public class ContainerNotSafeDemo {
    public static void main(String[] args) {
//        //list线程不安全
//        listNotSafe();
//        //set线程不安全
//        setNotSafe();
        //set线程不安全
        mapNotSafe();

    }

    public static void mapNotSafe() {
        //        Map<String,String> map=new HashMap<>();
//        Map<String,String> map=Collections.synchronizedMap(new HashMap<>());
        Map<String,String> map=new ConcurrentHashMap<>();
        for (int i = 1; i <= 30; i++) {
            new Thread(() -> {
                map.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0,8));
                System.out.println(map);
            }, String.valueOf(i)).start();
        }//Exception in thread "3" java.util.ConcurrentModificationException
    }

    public static void setNotSafe() {


//        Set<String> set=new HashSet<>();
//        Set<String> set=Collections.synchronizedSet(new HashSet<>());
        Set<String> set=new CopyOnWriteArraySet<>();
        for (int i = 1; i <= 30; i++) {
            new Thread(() -> {
                set.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(set);
            }, String.valueOf(i)).start();
        }
        //Exception in thread "19" java.util.ConcurrentModificationException


        new HashSet<>().add("a");
        /**
         HashSet<>()的底层是HashMap<>();
         public HashSet() {
         map = new HashMap<>();
         }
         public boolean add(E e) {
         return map.put(e, PRESENT)==null;
         }
         PRESENT的常量是HashMap的value.

         private static final Object PRESENT = new Object();

         */}

    public static void listNotSafe() {
        //初始值为10的空list,object类型的数组，根据泛型的对象确定可以使integer，user,扩容原值的一半，复制
         ArrayList<Integer> arrayList = new ArrayList<>();

        //List<String> list1= Arrays.asList("a","b","c");
        //list1.forEach(System.out::println);

        //List<String> list = new ArrayList<>();  //是线程不安全的   ArrayList（1.2）以牺牲多线程的安全为代价来提高并发行的
        //List<String> list =new Vector<>() ;    //Vector（1.1） add方法加锁了,数据一致性绝对可以保证，但是并发性下降
        //List<String> list=Collections.synchronizedList(new ArrayList<>());  //利用集合工具类Collections.synchronizedList构建安全的ArrayList
        List<String> list = new CopyOnWriteArrayList<>();


        //list.add("a");
        //list.add("b");
        //list.add("c");
        //for (String element : list) {
        //System.out.println(element);
        //}


        for (int i = 1; i <= 30; i++) {
            new Thread(() -> {
                list.add(UUID.randomUUID().toString().substring(0, 8));
                System.out.println(list);
            }, String.valueOf(i)).start();
        }
        //java.util.ConcurrentModificationException


        /**
         * 1 故障现象
         *        java.util.ConcurrentModificationException
         *
         * 2 导致原因
         *        并发争抢修改导致，参考我们的花名册签名情况，一个人正在写，另一个同学过来抢夺，导致数据不一致异常，也就是并发修改异常
         *
         * 3 解决方案
         *  List<String> list = new ArrayList<>();  //是线程不安全的   ArrayList（1.2）以牺牲多线程的安全为代价来提高并发行的
         * （3.1）List<String> list =new Vector<>() ;    //Vector（1.1） add方法加锁了,数据一致性绝对可以保证，但是并发性下降
         * （3.2）List<String> list=Collections.synchronizedList(new ArrayList<>());  //利用集合工具类Collections.synchronizedList构建安全的ArrayList
         * (3.3) List<String> list=new CopyOnWriteArrayList<>();
         * 4 优化建议（同样的错误不犯第二次）
         *
         */
        /**
         写时复制
         CopyOnWrite容器即写时复制的容器，往一个容器添加元素的时候，不直接往容器Object[]添加，而是先将Object[]进行copy,
         复制出一个新的容器 Object[] newElements 里添加元素，添加完元素之后，再将原容器的引用指向新容器setArraynew(Elements);
         这样做的好处是可以对 CopyOnWrite 容器进行并发的读，而不需要加锁，因为当前容器不会添加任何元素。所以 CopyOnWrite 容器
         也是一种读写分离的思想，读和写不同的容器
         CopyOnWriteArrayList
         jdk1.8
         public boolean add(E e) {
         final ReentrantLock lock=this.lock;
         lock.lock();
         try {
         Object[] es = getArray();
         int len = es.length;
         es = Arrays.copyOf(es, len + 1);
         es[len] = e;
         setArray(es);
         return true;
         }finally {
         lock.unlock();
         }
         }
         jdk12
         public boolean add(E e) {
         synchronized (lock) {
         Object[] es = getArray();
         int len = es.length;
         es = Arrays.copyOf(es, len + 1);
         es[len] = e;
         setArray(es);
         return true;
         }
         }
         */}
}




