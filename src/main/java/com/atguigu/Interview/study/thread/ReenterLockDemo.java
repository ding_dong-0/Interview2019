package com.atguigu.Interview.study.thread;


import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

//线程操作资源类
class Phone implements Runnable {
    public synchronized void sendSMS() {   //方法加synchronized，同步方法
        System.out.println(Thread.currentThread().getName() + "\t invoked sendSMS");
        sendEmail();
        set();
    }

    public synchronized void sendEmail() {   //方法加synchronized，同步方法
        System.out.println(Thread.currentThread().getName() + "\t ########invoked sendEmail");
    }


    Lock lock = new ReentrantLock();//默认是非公平锁

    @Override
    public void run() {
        get();
    }

    public void get() {
        lock.lock();
//        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + "\t invoked get()");
            set();
        } finally {
            lock.unlock();
//            lock.unlock();   //锁加几次解几次
        }

    }

    public void set() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + "\t ########invoked set()");
        } finally {
            lock.unlock();
        }

    }
}

/**
 * Description:
 * 可重入锁(也叫做递归锁)
 * 指的是同一先生外层函数获得锁后,内层敌对函数任然能获取该锁的代码
 * 在同一线程外外层方法获取锁的时候,在进入内层方法会自动获取锁
 * <p>
 * 也就是说,线程可以进入任何一个它已经标记的锁所同步的代码块
 * case one synchronized就是一个典型的可重入锁
 * t1	 invoked sendSMS
 * t1	 ########invoked sendEmail
 * t2	 invoked sendSMS
 * t2	 ########invoked sendEmail
 * <p>
 * <p>
 * case two synchronized就是一个典型的可重入锁
 */
public class ReenterLockDemo {
    public static void main(String[] args) {
        Phone phone = new Phone();
        new Thread(() -> {
            phone.sendSMS();
        }, "t1").start();

        new Thread(() -> {
            phone.sendSMS();
        }, "t2").start();
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println();
        System.out.println();
        System.out.println();

        Thread t3 = new Thread(phone,"t3");
        Thread t4 = new Thread(phone,"t4");
        t3.start();
        t4.start();
    }
}
