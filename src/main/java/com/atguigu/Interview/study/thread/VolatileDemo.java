package com.atguigu.Interview.study.thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

class MyData {     //MyData.java ===> MyData.class ===>JVM字节码
    volatile int number = 0;    //volatile 可见性是一种即时通知机制，当一个线程修改了主内存中的共享变量时，会立刻通知给其他线程

    public void addTo60() {
        this.number = 60;
    }

    //请注意，此时number前面是加了volatile关键字修饰的，Volatile的不保证原子性
    public /*synchronized*/ void addPlusPlus() {   //加synchronized可以帮助解决Volatile的原子性，但是synchronized时重锁
        number++;
    }

    AtomicInteger atomicInteger = new AtomicInteger();

    public void addMyAtmic() {

        atomicInteger.getAndIncrement();

    }

}

/**
 * 1  验证Volatile的可见性
 * -   1.1 int number=0；number变量之前根本没有添加Volatile关键字修饰，没有可见性
 * -   1.2 添加了volatile，可以解决可见性的问题
 * 2  验证Volatile的不保证原子性
 * -   2.1原子性指的是什么意思？
 * -      不可分割，完整性，也即某个线程正在做某个具体业务是，中间不可以被加塞或者被分割。需要整体完整，要么同时成功，要么同时失败。
 * -   2.2Volatile不保证原子性的案例演示
 * -   2.3why
 * -   2.4 如何解决原子性
 * *加synchronized
 * *使用我们的JUC下AtomicInteger
 */

public class VolatileDemo {
    public static void main(String[] args) {  //main是一切方法的入口
//        seeOkByVolatile();


        //Volatile不保证原子性的案例
        noAtomicByVolatile();


    }

    //    Volatile不保证原子性的案例
    public static void noAtomicByVolatile() {
        MyData myData = new MyData();

        for (int i = 1; i <= 20; i++) {
            new Thread(() -> {
                for (int j = 1; j <= 1000; j++) {
                    myData.addPlusPlus();
                    myData.addMyAtmic();
                }
            }, String.valueOf(i)).start();
        }

        //需要等待上面20 个线程都计算完成后，再用main线程取得最终的结果值是多少？
        //暂停一会线程
//        try {TimeUnit.SECONDS.sleep(5);} catch (InterruptedException e) {e.printStackTrace();}

        //多线程控制上面计算完成的时间的最好方法，大于2是因为后台默认有main线程和GC线程两个线程，
        while (Thread.activeCount() > 2) {
            Thread.yield();  //礼让线程
        }

        System.out.println(Thread.currentThread().getName() + "\t int type, finally number value: " + myData.number);
        System.out.println(Thread.currentThread().getName() + "\t AtomicInteger type,finally number value: " + myData.atomicInteger);
    }


    //Volatile可以保证可见性，即使通知其他线程，主物理内存的值已经被修改
    public static void seeOkByVolatile() {
        MyData myData = new MyData();  //资源类
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "\t come in");
            //暂停一会线程
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "\t updated number value:" + myData.number);
            myData.addTo60();

        }, "AAA").start();  //实现了runnable接口的lamda表达式

        //第二个线程就是我们的main线程
        while (myData.number == 0) {
            //main线程就一直在这里等待循环，直到number值不再等于零
        }
        System.out.println(Thread.currentThread().getName() + "\t mission is over，main get number value:" + myData.number);
    }


}
