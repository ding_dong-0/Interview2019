package com.atguigu.distributed.lock.common;

public class Client {
    public static void main(String[] args) {
//        OrderService orderService=new OrderService();
        for (int i = 1; i <= 50; i++) {
            new Thread(() -> {
                new OrderService().getOrderNumber();
            }, String.valueOf(i)).start();
        }
    }
}
