package com.atguigu.distributed.lock.common;

import com.atguigu.distributed.lock.t0401.ZkAbstractTemplateLock;
import com.atguigu.distributed.lock.t0401.ZkDistributedLock;
import com.atguigu.distributed.lock.util.OrderNumCreateUtil;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 多线程下用ReentrantLock（） 锁，保证不重复
 */
public class OrderService {
    private OrderNumCreateUtil orderNumCreateUtil = new OrderNumCreateUtil();
    private ZkAbstractTemplateLock zkLock = new ZkDistributedLock();

    public void getOrderNumber() {
        zkLock.zkLock();
        try {
            System.out.println("获得编号：----->:"+orderNumCreateUtil.getOrdNumber());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            zkLock.zkUnLock();
        }
    }
/*
    private Lock lock=new ReentrantLock();
    public String getOrderNumber(){
        lock.lock();
        try {
            return orderNumCreateUtil.getOrdNumber();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return null;
    }

 */


}
