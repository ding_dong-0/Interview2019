package com.atguigu.distributed.lock.t0401;

import org.I0Itec.zkclient.ZkClient;

import java.util.concurrent.CountDownLatch;

public abstract class ZkAbstractTemplateLock implements ZkLock {
    public static final String ZKSERVICE = "192.168.40.201:2181";
    public static final int TIME_OUT = 45 * 1000;
    ZkClient zkClient = new ZkClient(ZKSERVICE, TIME_OUT);
    protected String path="/zklock0401";
    protected CountDownLatch countDownLatch=null;

    @Override
    public void zkLock() {
        //模板设计模式
        if (tryZkLock()) {
            System.out.println(Thread.currentThread().getName() + "\t 占用锁成功");
        } else {
            waitZkLock();
            zkLock();
        }
    }

    public abstract void waitZkLock();

    public abstract boolean tryZkLock();

    @Override
    public void zkUnLock() {
        if (zkClient != null) {
            zkClient.close();
        }
        System.out.println(Thread.currentThread().getName() + "\t 释放锁成功");
        System.out.println();
        System.out.println();
    }
}
