package com.atguigu.distributed.lock.t0401;

public interface ZkLock {
    public void zkLock();
    public void zkUnLock();
}
