package com.atguigu.sh.test;

/**
 * java的加载机制：静态代码块>构造块>构造方法,静态代码块值加载一次，普通代码块每次都加载
 */
class Code {
    public Code() {
        System.out.println("Code的构造方法1111");
    }

    {
        System.out.println("Code的构造块2222");
    }

    static {
        System.out.println("Code的静态代码块3333");
    }
}

public class CodeBlock03 {
    {
        System.out.println("CodeBlock03的构造块444");
    }

    static {
        System.out.println("CodeBlock03的静态代码块555");
    }

    public CodeBlock03() {
        System.out.println("CodeBlock03的构造方法666");
    }

    public static void main(String[] args) {
        System.out.println("====================777");
        new Code();
        System.out.println("---------------------");
        new CodeBlock03();
        System.out.println("---------------------");
        new Code();
        System.out.println("---------------------");
        new CodeBlock03();
    }

}
