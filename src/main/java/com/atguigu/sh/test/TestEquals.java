package com.atguigu.sh.test;

import com.atguigu.Interview.entities.Person;

import java.util.HashSet;
import java.util.Set;

/**
 * ==和equals 谈谈理解
 * -     1、==既可以比较基本类型，也可以比较引用类型，如果比较基本类型比较值是否相等，如果比较引用类型比较地址是否相等
 * -       （凡是new出来的对象==比较结果一定为false）
 * -     2、equals是比较有两种情况，
 * -        （1）equals是Object中的方法，如果没有被重写过，就是==  比较的是引用类型（即地址）
 * -        （2）String覆写了equals方法，比较的是内容(具体根据覆写的equals方法定)
 *
 * HashSet的底层是HashSet,比较的的是hashcode值,String类同时也覆写了hashcode方法，
 * (HashSet存储的是hascode不能重复的) 集合类判断是不是同一个，看的是hashcode值
 *
 */

public class TestEquals {
    public static void main(String[] args) {
        String s1=new String("abc");
        String s2=new String("abc");

        int age=25;
        if(age==25){}

        //s1,s2 引用类型 ==可以比较引用类型
        System.out.println(s1==s2);  //false 对比的是地址《凡是new出来的对象==比较结果一定为false》
        System.out.println(s1.equals(s2));//true 对比的是内容
        Set<String> set01=new HashSet<>();
        set01.add(s1);
        set01.add(s2);
        System.out.println(s1.hashCode()+"\t"+s2.hashCode());
        System.out.println(set01.size());//1
        System.out.println("*****************");
        Person p1=new Person("abc");
        Person p2=new Person("abc");
        System.out.println(p1 == p2);//false  《凡是new出来的对象==比较结果一定为false》
        System.out.println(p1.equals(p2));//false
        Set<Person> set02=new HashSet<>();
        set02.add(p1);
        set02.add(p2);
        System.out.println(p1.hashCode()+"\t"+p2.hashCode());
        System.out.println(set02.size());
    }
}
