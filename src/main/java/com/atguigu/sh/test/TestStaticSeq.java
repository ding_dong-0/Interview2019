package com.atguigu.sh.test;

/**
 * 从父到子静态先行，主次分明主类先走
 */
class Father{
    public Father() {
        System.out.println(1);
    }
    {
        System.out.println(2);
    }
    static {
        System.out.println(3);
    }
}
class Son extends Father{
    public Son() {
        super();//默认的无参构造方法，不写、
        System.out.println(4);
    }
    {
        System.out.println(5);
    }
    static {
        System.out.println(6);
    }
}
public class TestStaticSeq {
    static {
        System.out.println(8);
    }
    public static void main(String[] args) {
        System.out.println(7);
        System.out.println("-------------");
        new Son();                 //从父到子静态先行，主次分明主类先走
        System.out.println("=============");
        new Son();
        System.out.println("=============");
        new Father();
    }
}
