package com.atguigu.sh.test;

import com.atguigu.Interview.entities.Person;

/**
 * 方法的参数传递机制
 * 考点
 * 1.java传值
 * 2.JVM变量分配
 * 3.变量及方法和作用域
 *
 */
public class TestTransferValue {
    public void changeValue1(int age) {
        age = 30;
    }

    public void changeValue2(Person person) {
        person.setPersonName("xxx");
    }

    public void changeValue3(String str) {
        str = "xxx";
    }

    public static void main(String[] args) {
        TestTransferValue test=new TestTransferValue();
        int age=20;
        test.changeValue1(age);
        System.out.println("age----"+age);//20
        Person person=new Person("abc");//等号左边是引用在java Stack中等号右边在堆中的Eden区
        test.changeValue2(person);
        System.out.println("person----"+person.getPersonName());
        String str="abc";   //字符串常量池
        test.changeValue3(str);
        System.out.println("string----"+str);
    }
}
