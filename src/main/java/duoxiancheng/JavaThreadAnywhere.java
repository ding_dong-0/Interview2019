package duoxiancheng;


public class JavaThreadAnywhere {
    public static void main(String[] args) {
        //获取当前的线程
        Thread currentThread = Thread.currentThread();
        //获取当前的线程的线程名称
        String currentThreadName = currentThread.getName();
        System.out.printf("The main method was executed by thread:%s",
                currentThreadName);
        System.out.println();
        Helper helper = new Helper("Java Thread anyWhere");
        helper.run();



    }

    static class Helper implements Runnable {
        private final String message;

        Helper(String message) {
            this.message = message;
        }


        @Override
        public void run() {
            doSomething(message);
        }

        private void doSomething(String message) {
            Thread currentThread = Thread.currentThread();
            String currentThreadName = currentThread.getName();
            System.out.printf("The doSomething method was executed by thread:%s", currentThreadName);
            System.out.println();
            System.out.println("Do something with " + message);
        }
    }

}
