package duoxiancheng;

public class RaceConditionDemo {
    public static void main(String[] args) {
        int numberOfThreads = args.length > 0 ? Short.valueOf(args[0]) : Runtime.getRuntime().availableProcessors();
        Thread[] workerThreads = new Thread[numberOfThreads];
        for (int i = 0; i < numberOfThreads; i++) {
            workerThreads[i] = new WorkerThreads(i, 10);
        }
        for (Thread ct : workerThreads) {
            ct.start();
        }

    }

    static class WorkerThreads extends Thread {
        private final int requestCount;

        WorkerThreads(int id, int requestCount) {
            super("worker-" + id);
            this.requestCount = requestCount;
        }

        @Override
        public void run() {
            int i = requestCount;
            String requestID;
        }
    }
}

