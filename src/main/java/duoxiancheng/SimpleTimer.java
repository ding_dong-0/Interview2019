package duoxiancheng;

public class SimpleTimer {
    private static int count;

    public static void main(String[] args) throws InterruptedException {
        System.out.println(args.length);
        count = args.length >= 1 ? Integer.valueOf(args[0]) : 60;
        int remaining;
        while (true) {
            remaining = countDown();
            if (0 == remaining) {
                break;
            } else {
                System.out.println("Remaining" + count + " secomd(s)");
            }
        }
        Thread.sleep(1000);
        System.out.println("Done");

    }

    private static int countDown() {
        return count--;
    }
}
