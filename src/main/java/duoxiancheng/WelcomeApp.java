package duoxiancheng;

public class WelcomeApp {
    public static void main(String[] args) {
        WelcomeThread welcomeThread = new WelcomeThread();
        welcomeThread.start();
        System.out.printf("1.Welocome! I'm %s.%n", Thread.currentThread().getName());
    }
}

//继承 Thread 类重写 run方法，开启一个线程
class WelcomeThread extends Thread {
    @Override
    public void run() {
        System.out.printf("2.Welocome! I'm %s.%n", Thread.currentThread().getName());
    }
}

