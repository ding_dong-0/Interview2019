package monky;

import jdk.nashorn.api.tree.NewTree;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Concurrent3 {
    static Object obj = new Object();
    static Lock lock = new ReentrantLock();

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            test2();
        }

    }

    public static void test1() {

        Thread t = new Thread(() -> {
            synchronized (obj) {
                try {
                    System.out.println("hello");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    public static void test2() {
        new Thread(() -> {
            lock.lock();
            try {
                System.out.println("hello");
                Thread.sleep(1000);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }


        }, "t").start();
    }

}
