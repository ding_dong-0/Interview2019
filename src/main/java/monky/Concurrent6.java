package monky;

/**
 * ThreadLocal（线程本地）线程之间是相互隔离的，每个线程都有自己单独的变量
 */

public class Concurrent6 {

    static ThreadLocal<Integer> threadLocal=new ThreadLocal<>();

    public static void main(String[] args) throws InterruptedException {
       Thread t1=new Thread(()->{
           System.out.println(threadLocal.get());
           threadLocal.set(0);
           System.out.println(threadLocal.get());
       });
        Thread t2=new Thread(()->{
            System.out.println(threadLocal.get());
            threadLocal.set(1);
            System.out.println(threadLocal.get());
        });
        t1.start();
        t1.join();
        t2.start();

    }
}
