package monky;

import lombok.Data;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class InitializerExamples {
    static int count;
    int i;

    //    static
    {
        System.out.println("Static Initializer");
//        i=6;
        count = count + 1;
        System.out.println("Count when Static Initializer is run is " + count);
    }


    private static void tokenize(String string, String regex) {
        String[] tokens = string.split(regex);
        System.out.println(Arrays.toString(tokens));
    }

    private static void tokenizeUsingScanner(String string, String regex) {
        Scanner scanner = new Scanner(string);
        scanner.useDelimiter(regex);
        ArrayList<String> matches = new ArrayList<>();
        while (scanner.hasNext()) {
            matches.add(scanner.next());
        }
        System.out.println(matches);
    }

    public static void main(String[] args) {
//        InitializerExamples example = new InitializerExamples();
//        InitializerExamples example2 = new InitializerExamples();
//        InitializerExamples example3 = new InitializerExamples();


//        tokenize("ac;bd;drf;e",";");

//        tokenizeUsingScanner("ac;bd;drf;e",";");
//
//        Date date = new Date();
//        date.setTime(date.getTime()+6*60*60*1000);
//        System.out.println(date);
//        System.out.println(DateFormat.getInstance().format(date));
//        System.out.println("==============");
//        Date date1 = new Date();
//        date.setTime(date1.getTime()-6*60*60*1000);
//        System.out.println(date);
//        System.out.println(DateFormat.getInstance().format(date));


//        Date date = new Date();
//        System.out.println(DateFormat.getDateInstance(DateFormat.FULL, new Locale("it", "IT")).format(date));
//        System.out.println(DateFormat.getDateInstance(DateFormat.FULL, Locale.ITALIAN).format(date));
//        System.out.println(DateFormat.getDateInstance(DateFormat.FULL).format(date));
//        System.out.println(DateFormat.getDateInstance().format(date));
//        System.out.println(DateFormat.getDateInstance(DateFormat.SHORT).format(date));
//        System.out.println(DateFormat.getDateInstance(DateFormat.MEDIUM).format(date));
//        System.out.println(DateFormat.getDateInstance(DateFormat.LONG).format(date));


//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.YEAR,2010);
//        calendar.set(Calendar.DATE,24);
//        calendar.set(Calendar.MONTH,8);
////        calendar.set(2010,8,24);
//        System.out.println(calendar.get(Calendar.YEAR));
//        System.out.println(calendar.get(Calendar.MARCH));
//        System.out.println(calendar.get(Calendar.DATE));
//        System.out.println(calendar.get(Calendar.WEEK_OF_MONTH));
//        System.out.println(calendar.get(Calendar.WEEK_OF_YEAR));
//        System.out.println(calendar.get(Calendar.DAY_OF_YEAR));
//        System.out.println(calendar.getFirstDayOfWeek());
//
//


//        System.out.println(NumberFormat.getIntegerInstance().format(321.24f));

//
//        List<Integer> integerList = Arrays.asList(1, 1, 2, 3, 4, 5, 9, 8, 8, 8, 9,10,26,88);
//        Stream<Integer> stream = integerList.stream().filter(i -> i > 3).distinct().limit(10).skip(1);
//        List<Integer> collect = stream.collect(Collectors.toList());
//        System.out.println(collect);

//        Stream<Integer> distinct = stream.distinct();
//        List<Integer> collect1 = distinct.collect(Collectors.toList());
//        System.out.println(collect1);

        int[] intArr = {1, 2, 3, 4, 5};
        IntStream stream1 = Arrays.stream(intArr);

        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);


        List<String> stringList = Arrays.asList("java8", "Lambdas", "in", "action");
        Stream<Integer> integerStream1 = stringList.stream().map(String::length);
        List<Integer> collect = integerStream1.collect(Collectors.toList());
        System.out.println(collect);


    }


}
