package monky;

import java.util.UUID;

public class StringBuilderDemo {
    public static void main(String[] args) throws InterruptedException {
        StringBuilder stringBuilder=new StringBuilder();
        for (int i = 1; i <= 10; i++) {
            new Thread(() -> {
                for (int j = 0; j <1000 ; j++) {
                    stringBuilder.append("a");
                }
            }, String.valueOf(i)).start();
        }
        Thread.sleep(10000);
        System.out.println(stringBuilder.length());
        StringBuffer stringBuffer=new StringBuffer();
    }

}
