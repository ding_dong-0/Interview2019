package 设计原则.开闭原则.improve;


public class Ocp {
    public static void main(String[] args) {
        //
        GraphicEditor graphicEditor = new GraphicEditor();
        graphicEditor.drawShape(new Rectangle());
        graphicEditor.drawShape(new Circle());
        graphicEditor.drawShape(new Triangle());
        graphicEditor.drawShape(new Hexagonal());
        graphicEditor.drawShape(new OtherGraphic());
    }
}

//这是一个用于绘图的类
class GraphicEditor {
    // 接收Shape 对象，调用draw()方法，来绘制不同的图形
    public void drawShape(Shape s) {
        s.draw();
    }
}

// Shape 类，基类
abstract class Shape {
    public abstract void draw();//抽象方法
}

class Rectangle extends Shape {
    @Override
    public void draw() {
        System.out.println(" 绘制矩形 ");
    }
}

class Circle extends Shape {
    @Override
    public void draw() {
        System.out.println(" 绘制圆形 ");
    }
}

// 新增画三角形
class Triangle extends Shape {
    @Override
    public void draw() {
        System.out.println(" 绘制三角形 ");
    }
}

// 新增画六边形
class Hexagonal extends Shape {
    @Override
    public void draw() {
        System.out.println(" 绘制六边形 ");
    }
}

//新增一个图形
class OtherGraphic extends Shape {
    @Override
    public void draw() {
        System.out.println(" 绘制其他图形 ");
    }
}