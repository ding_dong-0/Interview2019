package 设计原则.里氏替换原则;

public class Liskov {
    public static void main(String[] args) {
        A a = new A();
        System.out.println("11-3=" + a.func1(11, 3));
        System.out.println("1-8=" + a.func1(1, 8));
        System.out.println("====================================");
        B b = new B();
        System.out.println("11-3=" + b.func1(11, 3));
        System.out.println("1-8=" + b.func1(1, 8));
        System.out.println("11+3+9=" + b.func2(11, 3));
    }
}

// A 类
class A {
    // 返回两数的差
    public int func1(int num1, int num2) {
        return num1 - num2;
    }
}

//B 类继承了A类
class B extends A {
    // 增加了一个新的功能完成了两个数的相加，然后和9求和
    public int func1(int a, int b) {
        return a + b;
    }

    public int func2(int a, int b) {
        return func1(a, b) + 9;
    }
}