package 设计原则.里氏替换原则.improve;

public class Liskov {
    public static void main(String[] args) {
        A a = new A();
        System.out.println("11-3=" + a.func1(11, 3));
        System.out.println("1-8=" + a.func1(1, 8));
        System.out.println("====================================");
        B b = new B();
        //因为B类不再继承A类因此调用者不会再认为func1是求减法了
        //调用完成的功能就会很明确了
        System.out.println("11+3=" + b.func1(11, 3));//这里本意是求出 11+3
        System.out.println("1+8=" + b.func1(1, 8));// 1+8
        System.out.println("11+3+9=" + b.func2(11, 3));
        //使用组合仍然可以使用到A类的相关方法
        System.out.println("11-3=" + b.func3(11, 3));//这里本意是求出 11-3
    }
}

//创建一个更加基础的基类
class Base {
    //把更加基础的方法和成员写到Base类中
//    public int func1(int num1, int num2);
}


// A 类
class A extends Base {
    // 返回两数的差
    public int func1(int num1, int num2) {
        return num1 - num2;
    }
}

//B 类继承了A类
class B extends Base {
    //如果B需要使用A类的方法，使用组合关系
    private A a = new A();

    // 增加了一个新的功能完成了两个数的相加，然后和9求和
    public int func1(int a, int b) {
        return a + b;
    }

    public int func2(int a, int b) {
        return func1(a, b) + 9;
    }

    //我们让然想使用A的方法
    public int func3(int num1, int num2) {
        return a.func1(num1, num2);
    }

}