package 设计模式.享元模式.flyweight;

public class Client {
    public static void main(String[] args) {
        //创建一个工厂
        WebSiteFactory factory = new WebSiteFactory();
        //客户要一个以新闻形式发布的网站
        WebSite webSite1 = factory.getWebSiteCategory("新闻");
        User user = new User();
        user.setName("使用者1");
        webSite1.use(user);
        //客户要一个以博客形式发布的网站
        WebSite webSite2 = factory.getWebSiteCategory("博客");
        User user2 = new User();
        user2.setName("使用者2");
        webSite2.use(user2);
        //客户要一个以博客形式发布的网站
        WebSite webSite3 = factory.getWebSiteCategory("博客");
        User user3 = new User();
        user3.setName("使用者3");
        webSite3.use(user3);
        //客户要一个以博客形式发布的网站
        WebSite webSite4 = factory.getWebSiteCategory("博客");
        //使用构造方法
        webSite4.use(new User("使用者4"));

        System.out.println("网站的分类共=" + factory.getWebSiteCount());
    }
}
