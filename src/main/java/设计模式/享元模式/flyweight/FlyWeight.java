package 设计模式.享元模式.flyweight;

public class FlyWeight {
    public static void main(String[] args) {
//        IntergerDemo();
        Integer x = Integer.valueOf(-127);
        Integer z = Integer.valueOf(-127);
        System.out.println(x == z);


    }

    public static void IntergerDemo() {

        //如果 Integer.valueOf(x);  -127<=x<128 ，就是使用享元模式的返回，如果不在范围内，则仍然new
        //小结：
        //1、在 valueOf 方法中，先判断值是否在 IntegerCache 中，如果不在就创建新的  Integer（），否则就直接从缓存池返回
        //2、valueOf 方法就使用到享元模式
        //3、如果使用 valueOf 方法得到一个 Integer 实例，范围在  -127<=x<128  执行的速度比 new 要快


        Integer x = Integer.valueOf(127);  //得到 x 实例，类型 Integer
        Integer y = new Integer(127); //得到 y 实例，类型 Integer
        Integer z = Integer.valueOf(127);
        Integer w = new Integer(127);
        System.out.println(x.equals(y));  //大小，内容 true
        System.out.println(x == y);   //对象引用 false
        System.out.println(x == z);  // true
        System.out.println(w == x); //false
        System.out.println(w == y);//false
        /**
         * true
         * false
         * true
         * false
         * false
         */}
}
