package 设计模式.享元模式.flyweight;

public abstract class WebSite {
    public abstract void use(User user);
}
