package 设计模式.外观模式.facade;

public class Client {
    public static void main(String[] args) {
        //这里直接调。。  用很麻烦
        HomeTheaterFacade homeTheaterFacade = new HomeTheaterFacade();
        homeTheaterFacade.ready();
        System.out.println("---------------------");
        homeTheaterFacade.play();
        System.out.println("---------------------");
        homeTheaterFacade.pause();
        System.out.println("---------------------");
        homeTheaterFacade.end();

    }
}
