package 设计模式.外观模式.facade;

public class DVDPlayer {
    //使用单例模式,使用饿汉式
    private static DVDPlayer insatnce = new DVDPlayer();

    public static DVDPlayer getInstance() {
        return insatnce;
    }

    //构造方法私有化
    private DVDPlayer() {
    }

    public void on() {
        System.out.println("dvd on");
    }

    public void off() {
        System.out.println("dvd off");
    }

    public void play() {
        System.out.println("dvd is playing");
    }

    //...
    public void pause() {
        System.out.println(" dvd pause");
    }
}
