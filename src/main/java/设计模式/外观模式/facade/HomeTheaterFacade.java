package 设计模式.外观模式.facade;

public class HomeTheaterFacade {
    //定义各个子系统的对象
    private TheaterLight theaterLight;
    private Popcorn popcorn;
    private Stereo stereo;
    private Projector projector;
    private DVDPlayer dvdPlayer;
    private Screen screen;

    //构造器
    public HomeTheaterFacade() {
        this.theaterLight = TheaterLight.getInsatnce();
        this.popcorn = Popcorn.getInstance();
        this.stereo = Stereo.getInstance();
        this.projector = Projector.getInstance();
        this.dvdPlayer = DVDPlayer.getInstance();
        this.screen = Screen.getInsatnce();
    }

    //操作 分成4 步骤
    public void ready(){
        popcorn.on(); //爆米花机打开
        popcorn.pop(); //制作爆米花
        screen.down();  //屏幕放下
        projector.on(); //
        stereo.on(); //立体声打开
        dvdPlayer.on();//DVD 打开
        theaterLight.on();
        theaterLight.dim();//灯光调暗
    }

    public void play(){
        dvdPlayer.play();
    }
    public void pause(){
        dvdPlayer.pause();
    }

    public void end(){
        popcorn.off();
        theaterLight.bright();
        screen.up();
        projector.off();
        stereo.off();
        dvdPlayer.off();
    }

}
