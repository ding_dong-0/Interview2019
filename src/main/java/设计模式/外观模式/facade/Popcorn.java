package 设计模式.外观模式.facade;

public class Popcorn {
    private static Popcorn instance = new Popcorn();

    public static Popcorn getInstance() {
        return instance;
    }

    private Popcorn() {
    }

    public void on() {
        System.out.println("popcorn on");
    }

    public void off() {
        System.out.println("pocorn off");
    }

    public void pop() {
        System.out.println("popcorn is poping ");
    }
}
