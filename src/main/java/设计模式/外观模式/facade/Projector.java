package 设计模式.外观模式.facade;

public class Projector {
    private static Projector insatnce = new Projector();

    public static Projector getInstance() {
        return insatnce;
    }

    private Projector() {
    }

    public void on() {
        System.out.println("Projector on");
    }

    public void off() {
        System.out.println("Projector off");
    }
    public void focus(){
        System.out.println("Projector is projector");
    }
    //...
}
