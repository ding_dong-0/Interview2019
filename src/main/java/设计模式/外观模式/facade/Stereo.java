package 设计模式.外观模式.facade;

public class Stereo {
    public static Stereo instance=new Stereo();

    public static Stereo getInstance() {
        return instance;
    }

    private Stereo() {
    }

    public void on() {
        System.out.println("Stereo on");
    }

    public void off() {
        System.out.println("Stereo off");
    }
    public void up(){
        System.out.println("Stereo 音量调大");
    }
    public void down(){
        System.out.println("Stereo 音量调小");
    }
    //...
}
