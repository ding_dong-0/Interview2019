package 设计模式.外观模式.facade;

public class TheaterLight {
    private static TheaterLight insatnce=new TheaterLight();

    public static TheaterLight getInsatnce() {
        return insatnce;
    }

    private TheaterLight() {
    }
    public void on() {
        System.out.println("TheaterLight on");
    }

    public void off() {
        System.out.println("TheaterLight off");
    }

    public void dim() {
        System.out.println("TheaterLight dim..");
    }
    public void bright() {
        System.out.println("TheaterLight bright...");
    }
}
