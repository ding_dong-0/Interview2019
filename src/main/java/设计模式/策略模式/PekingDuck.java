package 设计模式.策略模式;

public class PekingDuck extends Duck {
    @Override
    public void display() {
        System.out.println("~~这是北京鸭");
    }

    //因为北京呀不能飞翔，因策需要重写 fly() 方法

    @Override
    public void fly() {
        System.out.println("北京鸭不能飞翔");
    }
}
