package 设计模式.策略模式;

import org.apache.logging.log4j.util.LambdaUtil;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Arrays.compare(.var)
 * 策略模式在JDK-Arrays 应用的源码的分析
 * 1）JDK的Arrays的 Comparator 就使用了策略模式
 * 2）代码分析+Debug源码+模式角色分析
 */
public class Strategy {
    public static void main(String[] args) {
        //数组
        Integer[] data = {9, 1, 2, 8, 4, 3};
        //实现升序排序，返回-1放左边，1，放右边，0保持不变
        //说明：
        //1.实现了 Comparator 接口(策略接口)，匿名类对象 new Comparator<Integer>(){...}
        //2.对象 new Comparator<Integer>(){...} 就是实现了策略接口的对象
        //3. public int compare(Integer o1, Integer o2) {} 制定具体的（操作方法）处理方式
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if (o1 > o2) {
                    return 1;
                } else {
                    return -1;
                }
            }
        };
        //说明：
        /**
         public static <T> void sort(T[] a, Comparator<? super T> c) {
         if (c == null) {
         sort(a);     //默认的方法
         } else {
         if (LegacyMergeSort.userRequested)
         legacyMergeSort(a, c);  //使用策略对象 c
         else
         // 使用策略对象 c
         TimSort.sort(a, 0, a.length, c, null, 0, 0);
         }
         }
         */
        // 方式1
        Arrays.sort(data, comparator);//第二个是策略对象
        System.out.println(Arrays.toString(data));//升序排列的

        //方式2 通过 Lambda 表达式实现 策略模式
        Integer[] data2 = {19, 11, 12, 18, 14, 13};
        Arrays.sort(data2, (var1, var2) -> {
            if (var1.compareTo(var2) > 0) {
                return -1;
            } else {
                return 1;
            }
        });
        System.out.println("data2=" + Arrays.toString(data2));
        Integer[] data3 = {191, 131, 142, 178, 214, 131};
        Arrays.sort(data2, (o1, o2) -> {
            if (o1 < o2) {
                return 1;
            } else {
                return -1;
            }
        });
        System.out.println("data3=" + Arrays.toString(data3));

    }
}


