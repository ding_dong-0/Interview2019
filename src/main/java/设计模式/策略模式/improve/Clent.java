package 设计模式.策略模式.improve;

public class Clent {
    public static void main(String[] args) {
        WildDuck wildDuck = new WildDuck();

        ToyDuck toyDuck = new ToyDuck();
        toyDuck.fly();
        toyDuck.quack();

        PekingDuck pekingDuck = new PekingDuck();
        pekingDuck.fly();
        pekingDuck.quack();

        //动态的改变耨个对象的行为
        PekingDuck pekingDuck1 = new PekingDuck();
        pekingDuck1.setFlyBehavior(new NoFlyBehavior());
        pekingDuck1.setQuackBehavor(new GeGeQuackBehavor());
        System.out.println("北京鸭的实际飞翔能力");
        pekingDuck1.fly();
        pekingDuck1.quack();

    }
}
