package 设计模式.策略模式.improve;

public abstract class Duck {
    //属性，策略接口
    FlyBehavior flyBehavior;
    QuackBehavor quackBehavor;
    //其他的属性<->策略接口关联的
    public Duck() {
    }

    public void setQuackBehavor(QuackBehavor quackBehavor) {
        this.quackBehavor = quackBehavor;
    }

    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public abstract void display();//显示鸭子的信息

    public void quack() {
       if (quackBehavor!=null){
           //改进
           quackBehavor.quack();
       }
    }

    public void swim() {
        System.out.println("鸭子会游泳~~");
    }

    public void fly() {
        //改进
        if(flyBehavior!=null){
            flyBehavior.fly();
        }
    }


}
