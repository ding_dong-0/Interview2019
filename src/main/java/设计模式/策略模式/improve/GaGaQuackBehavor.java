package 设计模式.策略模式.improve;

public class GaGaQuackBehavor implements QuackBehavor {
    @Override
    public void quack() {
        System.out.println("~~GaGa 叫");
    }
}
