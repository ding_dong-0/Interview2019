package 设计模式.策略模式.improve;

public class NoQuackBehavor implements QuackBehavor {
    @Override
    public void quack() {
        System.out.println("不会叫");
    }
}
