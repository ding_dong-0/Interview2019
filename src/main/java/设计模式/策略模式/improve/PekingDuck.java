package 设计模式.策略模式.improve;

public class PekingDuck extends Duck {

    //假如北京鸭可以飞翔，但是飞向技术一般
    public PekingDuck() {
        flyBehavior = new BadFlyBehavior();
        quackBehavor = new GaGaQuackBehavor();
    }

    @Override
    public void display() {
        System.out.println("~~这是北京鸭");
    }


}
