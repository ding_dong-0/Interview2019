package 设计模式.策略模式.improve;

public interface QuackBehavor {
    void quack();//子类去实现
}
