package 设计模式.策略模式.improve;

public class ToyDuck extends Duck {
    public ToyDuck() {
        flyBehavior=new NoFlyBehavior();
        quackBehavor=new NoQuackBehavor();
    }

    @Override
    public void display() {
        System.out.println("玩具鸭");
    }
    //需要重写父类的所有方法
    @Override
    public void quack() {
        System.out.println("玩具鸭不能叫~~");
    }
    @Override
    public void swim() {
        System.out.println("玩具鸭不会游泳~~");
    }

}
