package 设计模式.策略模式.improve;

public class WildDuck extends Duck {
    //构造器，传入 FlyBehavior 的对象


    public WildDuck() {
        flyBehavior = new GoodFly();
        quackBehavor = new GaGaQuackBehavor();
    }

    @Override
    public void display() {
        System.out.println("这是野鸭");
    }
}
