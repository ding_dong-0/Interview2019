package 设计模式.组合模式.composite;

import java.util.HashMap;

public class Client {
    public static void main(String[] args) {
        //从大到小创建对象 学校
        OrganizationComponent university = new University("清华大学", "中国顶级的大学");
        //创建 学院
        OrganizationComponent coputerCollege = new College("计算机学院", "计算机学院");
        OrganizationComponent infoEngineerCollege = new College("信息工程学院", "信息工程学院");
        //将学院加入到 university
        university.add(coputerCollege);
        university.add(infoEngineerCollege);


        //创建各个学院下面的系（专业）
        coputerCollege.add(new Department("软件工程","软件工程不错"));
        coputerCollege.add(new Department("网络工程","网络工程不错"));
        coputerCollege.add(new Department("计算机科学与技术","计算机科学与技术是一个老牌的专业"));

        //
        infoEngineerCollege.add(new Department("通信工程","通信工程不好学"));
        infoEngineerCollege.add(new Department("信息工程","信息工程好学"));


//        university.print();
//        coputerCollege.print();
        infoEngineerCollege.print();


    }
}
