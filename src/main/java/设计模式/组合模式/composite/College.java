package 设计模式.组合模式.composite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class College extends OrganizationComponent {

    // List 存放的是 Department
    List<OrganizationComponent> organizationComponents = new ArrayList<>();

    // 构造器
    public College(String name, String des) {
        super(name, des);
    }

    // 重写 add 方法
    @Override
    protected void add(OrganizationComponent organizationComponent) {
        // 将来实际业务中 ，College 的add 方法和 University add 方法不一定完全一样
        organizationComponents.add(organizationComponent);
    }

    //重写 remove 方法
    @Override
    protected void remove(OrganizationComponent organizationComponent) {
        organizationComponents.remove(organizationComponent);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getDes() {
        return super.getDes();
    }

    // print 方法，就是输出 University 所包含的学院
    @Override
    protected void print() {
        System.out.println("-------------" + getName() + "----------------");
        for (OrganizationComponent organizationComponent : organizationComponents) {
            organizationComponent.print();
        }
    }
}
