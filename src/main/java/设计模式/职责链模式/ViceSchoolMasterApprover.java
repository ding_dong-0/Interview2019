package 设计模式.职责链模式;

public class ViceSchoolMasterApprover extends Approver{
    public ViceSchoolMasterApprover(String name) {
        super(name);
    }

    @Override
    public void processRequest(PurchaseRequest purchaseRequest) {
        if (10000 < purchaseRequest.getPrice() && purchaseRequest.getPrice() <= 30000) {
            System.out.println("金额："+purchaseRequest.getPrice()+"在10000-30000范围，在副校长的权限范围内");
            System.out.println("请求编号id=" + purchaseRequest.getId() + "被" + this.name + "处理");
        } else {
            approver.processRequest(purchaseRequest);
        }
    }
}
