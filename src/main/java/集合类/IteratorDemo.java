package 集合类;

import java.util.ArrayList;
import java.util.Iterator;

public class IteratorDemo {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        Iterator<Integer> iterator = list.iterator();

        System.out.println("======================");

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        /*
        for (; iterator.hasNext(); ) {
            System.out.println(iterator.next() + "pp");
        }
         */
    }
}
