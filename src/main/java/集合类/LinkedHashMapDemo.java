package 集合类;

import java.util.*;

/**
 * LinkedHashMap是继承于HashMap，是基于HashMap和双向链表来实现的。
 * HashMap无序；LinkedHashMap有序，可分为插入顺序和访问顺序两种。如果是访问顺序，那put和get操作已存在的Entry时，都会把Entry移动到双向链表的表尾(其实是先删除再插入)。
 * LinkedHashMap存取数据，还是跟 HashMap 一样使用的 Entry[] 的方式，双向链表只是为了保证顺序。
 * LinkedHashMap 是线程不安全的。
 * <p>
 * 插入顺序和访问顺序。
 * LinkedHashMap默认的构造参数是默认  插入顺序的，就是说你插入的是什么顺序，读出来的就是什么顺序，
 * 但是也有访问顺序，就是说你访问了一个key，这个key就跑到了最后面
 * <p>
 * 这里accessOrder设置为false，表示不是访问顺序而是插入顺序存储的，这也是默认值，表示LinkedHashMap中存储的顺序是
 * 按照调用put方法插入的顺序进行排序的。LinkedHashMap也提供了可以设置accessOrder的构造方法，我们来看看这种模式下，它的顺序有什么特点？
 */
public class LinkedHashMapDemo {

    public static void main(String[] args) {
//        hashMapDemo();
//        linkedHashMapdemo();
//        linkedHashMapOrder();
        treeMapDemo();


    }

    public static void treeMapDemo() {
        // TreeMap的用法（主要是排序）  TreeMap中默认的排序为升序，如果要改变其排序可以自己写一个Comparator
        TreeMap<String, Integer> map = new TreeMap<>(new xbComparator()); //new TreeMap<>();
        map.put("key_6", 6);
        map.put("key_1", 1);
        map.put("key_4", 4);
        map.put("key_2", 2);
        map.put("key_5", 5);
        map.put("key_3", 3);
        //        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        Set<String> keys = map.keySet();
        Iterator<String> iterator = keys.iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            System.out.println(" " + key + ":" + map.get(key));
        }
    }

    public static void linkedHashMapOrder() {
        // 第三个参数用于指定 accessOrder 值
        LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<String, String>(16, 0.75f, true);//new LinkedHashMap<>();
        linkedHashMap.put("name111", "josan111");
        linkedHashMap.put("name222", "josan222");
        linkedHashMap.put("name333", "josan333");
        System.out.println("开始时顺序：");
        Set<Map.Entry<String, String>> set = linkedHashMap.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println("key:" + key + ",value:" + value);
        }
        System.out.println("通过 get 方法，导致 key 为 name1 对应的 Entry 到表尾");
        linkedHashMap.get("name111");
        Set<Map.Entry<String, String>> set2 = linkedHashMap.entrySet();
        Iterator<Map.Entry<String, String>> iterator2 = set2.iterator();
        while (iterator2.hasNext()) {
            Map.Entry<String, String> entry = iterator2.next();
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println("key:" + key + ",value:" + value);
        }

        /**
         * 因为调用了get("name1")导致了name1对应的Entry移动到了最后，这里只要知道LinkedHashMap有插入顺序和访问顺序两种就可以
         *
         * 开始时顺序：
         * key:name111,value:josan111
         * key:name222,value:josan222
         * key:name333,value:josan333
         * 通过 get 方法，导致 key 为 name1 对应的 Entry 到表尾
         * key:name111,value:josan111
         * key:name222,value:josan222
         * key:name333,value:josan333
         */}

    /**
     * LinkedHashMap是有序的，且默认为插入顺序。
     */
    public static void linkedHashMapdemo() {
        LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("name44", "josan44");
        linkedHashMap.put("name11", "josan11");
        linkedHashMap.put("name88", "josan88");
        linkedHashMap.put("name22", "josan22");
        linkedHashMap.put("name33", "josan33");
        Set<Map.Entry<String, String>> set = linkedHashMap.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println("key:" + key + ",value:" + value);
        }
        /**
         * 得到的结果：
         * key:name44,value:josan44
         * key:name11,value:josan11
         * key:name88,value:josan88
         * key:name22,value:josan22
         * key:name33,value:josan33
         */
    }


    /**
     * 插入顺序和访问顺序
     * HashMap是无序的，当我们希望有顺序地去存储key-value时，就需要使用LinkedHashMap了。
     */
    public static void hashMapDemo() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("name1", "josan1");
        hashMap.put("name2", "josan2");
        hashMap.put("name5", "josan5");
        hashMap.put("name3", "josan3");
        hashMap.put("name0", "josan0");
        hashMap.put("name4", "josan4");
        Set<Map.Entry<String, String>> set = hashMap.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println("key:" + key + ",value:" + value);
        }
        /**
         * HashMap是无序的，当我们希望有顺序地去存储key-value时，就需要使用LinkedHashMap了。
         * 我们是按照xxx1、xxx2、xxx5、xxx3、xxx0、xxx4的顺序插入的，但是输出结果并不是按照顺序的。
         * 得到的结果：
         * key:name5,value:josan5
         * key:name4,value:josan4
         * key:name3,value:josan3
         * key:name2,value:josan2
         * key:name1,value:josan1
         * key:name0,value:josan0
         */
    }
}


/**
 * LinkedHashMap 全参构造方法
 * public LinkedHashMap(int initialCapacity,
 * float loadFactor,
 * boolean accessOrder) {
 * super(initialCapacity, loadFactor);
 * this.accessOrder = accessOrder;
 * }
 * <p>
 * 参数说明：
 * <p>
 * initialCapacity   初始容量大小，使用无参构造方法时，此值默认是16
 * loadFactor       加载因子，使用无参构造方法时，此值默认是 0.75f
 * accessOrder   false： 基于插入顺序     true：  基于访问顺序
 * 重点看看accessOrder的作用，使用无参构造方法时，此值默认是false。
 * 此值为 true，基于访问的顺序，get一个元素后，这个元素被加到最后(使用了LRU 最近最少被使用的调度算法)
 * https://www.cnblogs.com/yejg1212/archive/2013/04/01/2992921.html
 */


class xbComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        String i1 = (String) o1;
        String i2 = (String) o2;
        return -i1.compareTo(i2);
    }
}
