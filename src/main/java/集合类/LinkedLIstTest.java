package 集合类;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedLIstTest {
    public static void main(String[] args) {
        LinkedList<String> a = new LinkedList<>();
        a.add("Amy");
        a.add("Erica");
        a.add("Carl");
        System.out.println(a);
        LinkedList<String> b = new LinkedList<>();
        b.add("Bob");
        b.add("Gloria");
        b.add("Frances");
        b.add("Doug");
        b.add("Bog");
        System.out.println(b);
        ListIterator<String> aIter = a.listIterator();
        Iterator<String> bIter = b.iterator();

        while (bIter.hasNext()) {
            if (aIter.hasNext()) aIter.next();
            aIter.add(bIter.next());
        }
        System.out.println(a);
        System.out.println("====================");
        System.out.println(b);
        bIter = b.iterator();
        while (bIter.hasNext()) {
            bIter.next();// skip one elemet
            if (bIter.hasNext()) {
                bIter.next();    //跳过一个元素
                bIter.remove();  //移除一个元素
            }
        }
        System.out.println(b);
        System.out.println("====================");

        a.removeAll(b);//删除集合 a 中包含的集合 b 元素
        System.out.println(a);


    }
}
