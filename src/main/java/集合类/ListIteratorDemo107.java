package 集合类;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * ListIterator 可以实现逆序遍历列表中的元素
 */
public class ListIteratorDemo107 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        ListIterator<Integer> li = list.listIterator();
        while (li.hasNext()) {
            li.next();
        }
        while (li.hasPrevious()) {
            System.out.println(li.previous() + " ");
        }
        System.out.println("=========================");
        for (li = list.listIterator(); li.hasNext(); ) {
            li.next();
        }
        for (; li.hasPrevious(); ) {
            System.out.println(li.previous() + " ");
        }

    }
}
