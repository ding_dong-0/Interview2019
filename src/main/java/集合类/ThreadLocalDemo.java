package 集合类;
/**
 * ThreadLocal 应用场景
 * ThreadLocal 的特性也导致了应用场景比较广泛，主要的应用场景如下：
 *
 * 线程间数据隔离，各线程的 ThreadLocal 互不影响
 * 方便同一个线程使用某一对象，避免不必要的参数传递
 * 全链路追踪中的 traceId 或者流程引擎中上下文的传递一般采用 ThreadLocal
 * Spring 事务管理器采用了 ThreadLocal
 * Spring MVC 的 RequestContextHolder 的实现使用了 ThreadLocal
 */

/**
 * ThreadLocal 是 Java 里一种特殊变量，它是一个线程级别变量，每个线程都有一个 ThreadLocal 就是每个线程都拥有了自己独立的一个变量，竞态条件被彻底消除了，在并发模式下是绝对安全的变量。
 * 可以通过 ThreadLocal<T> value = new ThreadLocal<T>(); 来使用。
 * 会自动在每一个线程上创建一个 T 的副本，副本之间彼此独立，互不影响，可以用 ThreadLocal 存储一些参数，以便在线程中多个方法中使用，用以代替方法传参的做法。
 */
public class ThreadLocalDemo {
    /**
     * ThreadLocal 变量，每个线程都有一个副本，互不干扰
     * <p>
     * static final 定义了一个 THREAD_LOCAL 变量，其中 static 是为了确保全局只有一个保存 String 对象的 ThreadLocal 实例；
     * final 确保 ThreadLocal 的实例不可更改，防止被意外改变，导致放入的值和取出来的不一致，另外还能防止 ThreadLocal 的内存泄漏。
     */

    public static final ThreadLocal<String> THREAD_LOCAL = new ThreadLocal<>();

    public void threadLocalTest() throws InterruptedException {
        // 主线程设置值
        THREAD_LOCAL.set("wupx");
        String v = THREAD_LOCAL.get();
        System.out.println("Thread-0线程执行之前，" + Thread.currentThread().getName() + "线程取到的值：" + v);

        new Thread(new Runnable() {
            @Override
            public void run() {
                String v = THREAD_LOCAL.get();
                System.out.println(Thread.currentThread().getName() + "\t 线程取到的值：" + v);
                // 设置 threadLocal
                THREAD_LOCAL.set("huxy");
                v = THREAD_LOCAL.get();
                System.out.println("重新设置之后" + Thread.currentThread().getName() + "\t 线程取到的值：" + v);
                System.out.println(Thread.currentThread().getName() + "\t 线程执行结束");
            }
        }).start();
        Thread.sleep(3000l);
        System.out.println("**********************************");
        new Thread(() -> {
            String s = THREAD_LOCAL.get();
            System.out.println(Thread.currentThread().getName() + "\t 线程取到的值：" + s);
            THREAD_LOCAL.set("侯赢超");
            s = THREAD_LOCAL.get();
            System.out.println("重新设置之后" + Thread.currentThread().getName() + "\t 线程取到的值：" + s);
            System.out.println(Thread.currentThread().getName() + "\t 线程执行结束");
        }).start();
        Thread.sleep(3000l);
        System.out.println("**********************************");
        //等待所有线程执行结束
        Thread.sleep(3000l);
        v = THREAD_LOCAL.get();
        System.out.println("Thread-0线程执行之后，" + Thread.currentThread().getName() + "\t 线程取到的值：" + v);


    }

    /**
     * Thread-0 线程执行之前，先给 THREAD_LOCAL 设置为 wupx，然后可以取到这个值，然后通过创建一个新的线程以后去取这个值，发现新线程取到的为
     * null，意外着这个变量在不同线程中取到的值是不同的，不同线程之间对于 ThreadLocal 会有对应的副本，接着在线程 Thread-0 中执行对
     * THREAD_LOCAL 的修改，将值改为 huxy，可以发现线程 Thread-0 获取的值变为了 huxy，主线程依然会读取到属于它的副本数据 wupx，这就是线程的封闭。
     *
     * @param args
     * @throws InterruptedException
     */

    public static void main(String[] args) throws InterruptedException {
        new ThreadLocalDemo().threadLocalTest();
    }

}
