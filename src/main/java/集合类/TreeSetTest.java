package 集合类;

import java.util.TreeSet;

public class TreeSetTest {
    public static void main(String[] args) {
        TreeSet<String> sorter = new TreeSet<>();
        sorter.add("Bob");
        sorter.add("Amy");
        sorter.add("Carl");
        sorter.forEach(s -> System.out.println(s));
        System.out.println("=====================");
        for (String s : sorter) {
            System.out.println(s);
        }
    }
}
