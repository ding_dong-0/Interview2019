package bat.ke.qq.com;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * 使用线程的方式将10000个数放进List集合
 */
public class ThreadPkTest {
    public static void main(String[] args) throws InterruptedException {
        Long start = System.currentTimeMillis();
        final List<Integer> l = new ArrayList<>();
        final Random random = new Random();
        for (int i = 0; i < 10000; i++) {
            Thread thread = new Thread() {
                public void run() {
                    l.add(random.nextInt());
                }
            };
            thread.start();
            thread.join();//主线程会等待其他线程都执行完后一起结束
        }
        System.out.println("时间："+(System.currentTimeMillis()-start));
        System.out.println(l.size());
    }
}
